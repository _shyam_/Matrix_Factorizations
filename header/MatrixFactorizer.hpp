#ifndef __MatrixFactorizer_hpp__
#define __MatrixFactorizer_hpp__

#include "MatrixData.hpp"
#include "MatrixFactorizations/getPseudoInverse.hpp"
#include "MatrixFactorizations/getCUR.hpp"
#include "MatrixFactorizations/getRandomizedSVD.hpp"
#include "MatrixFactorizations/getACA.hpp"
#include "MatrixFactorizations/getRRQR.hpp"
#include "MatrixFactorizations/getID.hpp"

#endif
